/*
  ########################################################################################################

  MSTclust: Minimum Spanning Tree-based clustering
  
  Copyright (C) 2020  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/
/*
  ########################################################################################################
  ## v0.21.200603ac
  +  man updated

  ## v0.2.200601ac
  +  new option -g to compute silhouettes using generalized mean with any integer exponent
  ########################################################################################################
*/

import java.io.*;
import java.util.*;

public class MSTclust {

    //### constants  ###############################################################################################
    final static String VERSION = "0.21.200528ac";
    final static String NOTHING = "N.o./.T.h.I.n.G";
    final static short EMPTY = 0;
    final static String BLANK = "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ";
    final static double EPSILON = 0.000000001; //## NOTE: little constant to increase the cutoff value
    final static double DELTA = 0.00000000001; //## NOTE: little constant to deal with zero-values when computing geometric means 
    final static double CI_MIN = 0.025;
    final static double CI_MAX = 0.975;
    final static int NINF = Integer.MIN_VALUE;
    final static int PINF = Integer.MAX_VALUE;

    //### io  ######################################################################################################
    static BufferedReader in;
    static BufferedWriter out;
    static StringBuilder outcaption;  // tab-delimited output caption
    static StringBuilder outstats;    // tab-delimited output stats

    //### options  #################################################################################################
    static File infile;               // option -i ; either tab-delimited profiles or distance matrix (mandatory); when profiles, only positive values < 65535
    static String basename;           // option -o ; basename for output files (mandatory)
    static String lfld;               // option -l ; for tab-delimited profiles: field(s) containing profile labels (default: "1")
    static String pfld;               // option -p ; for tab-delimited profiles: field(s) containing profiles (default: "2-");
    static String rows;               // option -r ; for tab-delimited profiles: raw list (default: "1-");
    static double missp;              // option -e ; for tab-delimited profiles: max allowed proportion of empty entries per profile (default: 0.25)
    static boolean hctree;            // option -h ; hierarchical tree (default: not set)
    static boolean mstree;            // option -m ; minimum spanning tree (default: not set)
    static double cutoff;             // option -c ; cutoff value for clustering (default: -1)
    static int exponent;              // option -g ; generalized mean exponent value for estimating silhouettes (default: 1)
    static boolean outtab;            // option -t ; to output stats in tab-delimited format (default: not set)
    static int nloc;                  // option -L ; no. loci for noising (default: 0)
    static int bins;                  // option -B ; no. bins for subsampling (default: 0)
    static int reps;                  // option -R ; no. replicates (default: 100)
    static long seed;                 // option -S ; seed value (default: 0)
    
    //### data  ####################################################################################################
    static boolean dfile;             // true if infile contains a distance matrix
    static int n;                     // no. profiles
    static int lgt;                   // profile size
    static BitSet blfld;              // label field ids
    static ArrayList<String> lbl;     // profile labels
    static int maxlbl;                // max label length
    static BitSet bpfld;              // profile field ids
    static short[][] pr;              // profile array
    static int[] missing;             // no. missing entries par profile
    static BitSet brows;              // raw ids
    static float[][] dm;              // lower-triangular distance matrix
    static int[] clust;               // partition array
    static int k;                     // no. class(es)
    static int[] csize;               // size of each class
    static double[] silh;             // silhouette indexes
    static double[] csilh;            // avg silhouette for each class
    static int[][] srt;               // clustered elements, sorted according to silhouette
    static double silhouette;         // avg silhouette of the clustering
    static Random random;             // stream of pseudorandom numbers
    static double[] aurand;           // an array of lgt+1 sorted random numbers drawn from ]0, 1[
    
    //### stuffs  ##################################################################################################
    static short a;
    static int b, c, i, j, o, r, x, y, a_i, a_j, card, x025, x975;
    static float dij;
    static double up, dn, lo, av, step, rate;
    static String line;
    static StringBuilder sb;
    static short[] si;
    static int[] ai, subsample, noiclust, subclust, clustsub, mm;
    static int[][] a2i;
    static float[][] sdm;
    static double[] rsi, rw1, rw2, aw, xaxis;
    static double[][] a2d, ssi, sw1, sw2;
    static String[] aS, aS2;
    static ArrayList<Integer> alI;
   
    public static void main(String[] args) throws IOException {

	//##########################################################################################################
	//### man                                                                                                ###
	//##########################################################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" MSTclust v." + VERSION);
	    System.out.println("");
	    System.out.println(" Minimum Spanning Tree-based clustering");
	    System.out.println("");
	    System.out.println(" USAGE:  MSTclust  -i <infile>  -o <basename>  [options]");
	    System.out.println("");
	    System.out.println(" OPTIONS:");
	    System.out.println("");
	    System.out.println(" -i <infile>    input file  containing  either tab-delimited  profiles or a");
	    System.out.println("                lower-triangular distance matrix (mandatory)");
	    System.out.println(" -o <basenmae>  basename for output files (mandatory)");
	    System.out.println(" -r <string>    selecting only specified rows (default: \"1-\")");
	    System.out.println(" -l <string>    (input tab-delimited profiles)  field(s) containing profile");
	    System.out.println("                labels (default: \"1\")");
	    System.out.println(" -p <string>    (input tab-delimited profiles)  fields  containing profiles");
	    System.out.println("                (default: \"2-\")");
	    System.out.println(" -e <float>     (input tab-delimited profiles)  maximum allowed  proportion");
	    System.out.println("                of empty entries per profile (default: 0.25)");
	    System.out.println(" -c <float>     inclusive cutoff to define cluster(s) (default: not set)");
	    System.out.println(" -g <integer>   generalized  mean   exponent   for  computing  silhouettes;");
	    System.out.println("                negative or positive infinity can be set using -inf or inf,");
	    System.out.println("                respectively (default: 1  i.e. arithmetic mean)");
	    System.out.println(" -S <integer>   seed value for  data perturbation  and subsampling analyses");
	    System.out.println("                (default: 0)");
	    System.out.println(" -L <integer>   profile length  to  carry out  data  perturbation  analysis");
	    System.out.println("                (default: not set)");
	    System.out.println(" -B <integer>   number  of  bins to  carry out  data  subsampling  analyses");
	    System.out.println("                (default: not set)");
	    System.out.println(" -R <integer>   number of replicates for  data perturbation and subsampling");
	    System.out.println("                analyses (default: 100)");
	    System.out.println(" -t             tab-delimited results printed in stdout (default: not set)");
	    System.out.println(" -h             writing a  single-linkage  hierarchical classification tree");
	    System.out.println("                into an output file (default: not set)");
	    System.out.println(" -m             writing  a  minimum  spanning  tree  into  an  output  file");
	    System.out.println("                (default: not set)");
	    System.out.println("");
	    System.exit(0);
	}


	//##########################################################################################################
	//### reading options                                                                                    ###
	//##########################################################################################################
	infile = new File(NOTHING);  // option -i
	basename = NOTHING;          // option -o
	outtab = false;              // option -t
	lfld = "1";                  // option -l
	pfld = "2-";                 // option -p
	missp = 0.25;                // option -e
	rows = "1-";                 // option -r
	hctree = false;              // option -h
	mstree = false;              // option -m
	cutoff = -1;                 // option -c
	exponent = 1;                // option -g
	nloc = 0;                    // option -L
	bins = 0;                    // option -B
	reps = 100;                  // option -R
	seed = 0;                    // option -S
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") ) { infile = new File(args[++o]);                                                                                                                            continue; }
	    if ( args[o].equals("-o") ) { basename = args[++o];                                                                                                                                    continue; }
	    if ( args[o].equals("-l") ) { lfld = args[++o];                                                                                                                                        continue; }
	    if ( args[o].equals("-p") ) { pfld = args[++o];                                                                                                                                        continue; }
	    if ( args[o].equals("-r") ) { rows = args[++o];                                                                                                                                        continue; }
	    if ( args[o].equals("-t") ) { outtab = true;                                                                                                                                           continue; }
	    if ( args[o].equals("-h") ) { hctree = true;                                                                                                                                           continue; }
	    if ( args[o].equals("-m") ) { mstree = true;                                                                                                                                           continue; }
	    if ( args[o].equals("-e") ) { try { missp = Double.parseDouble(args[++o]); }  catch ( NumberFormatException e ) { System.err.println("incorrect value (option -e)"); System.exit(1); } continue; }
	    if ( args[o].equals("-c") ) { try { cutoff = Double.parseDouble(args[++o]); } catch ( NumberFormatException e ) { System.err.println("incorrect value (option -c)"); System.exit(1); } continue; }
	    if ( args[o].equals("-L") ) { try { nloc = Integer.parseInt(args[++o]); }     catch ( NumberFormatException e ) { System.err.println("incorrect value (option -L)"); System.exit(1); } continue; }
	    if ( args[o].equals("-B") ) { try { bins = Integer.parseInt(args[++o]); }     catch ( NumberFormatException e ) { System.err.println("incorrect value (option -B)"); System.exit(1); } continue; }
	    if ( args[o].equals("-R") ) { try { reps = Integer.parseInt(args[++o]); }     catch ( NumberFormatException e ) { System.err.println("incorrect value (option -R)"); System.exit(1); } continue; }
	    if ( args[o].equals("-S") ) { try { seed = Long.parseLong(args[++o]); }       catch ( NumberFormatException e ) { System.err.println("incorrect value (option -S)"); System.exit(1); } continue; }
	    if ( args[o].equals("-g") ) {
		line=args[++o].toLowerCase();
		if ( line.equals("inf") || line.equals("+inf") ) { exponent = PINF;                                                                                                                continue; }
		if ( line.equals("-inf") )                       { exponent = NINF;                                                                                                                continue; }
		try { exponent = Integer.parseInt(line); }                                catch ( NumberFormatException e ) { System.err.println("incorrect value (option -g)"); System.exit(1); } continue; }
	}
	//### testing mandatory options -i, and -o
	if ( infile.toString().equals(NOTHING) ) { System.err.println("input file not specified (option -i)");             System.exit(1); }
	if ( ! infile.exists() )                 { System.err.println("input file does not exist (option -i)");            System.exit(1); }
	if ( basename.equals(NOTHING) )          { System.err.println("output file basename not specified (option -o)");   System.exit(1); }
	//### testing option -e, -L, -B and -R
	if ( missp < 0 )                         { System.err.println("incorrect value (option -e)");                      System.exit(1); } 
	if ( nloc < 0 )                          { System.err.println("incorrect value (option -L)");                      System.exit(1); } 
	if ( bins < 0 || bins == 1 )             { System.err.println("incorrect value (option -B)");                      System.exit(1); } 
	if ( reps <= 0 )                         { System.err.println("incorrect value (option -R)");                      System.exit(1); } 
	
	
	//##########################################################################################################
	//### init                                                                                               ###
	//##########################################################################################################
	lbl = new ArrayList<String>();                             // label names
	maxlbl = 0;                                                // label name max length
	dm = new float[1][];                                       // distance matrix
	outcaption = new StringBuilder("");                        // tab-delimited output caption
	outstats = new StringBuilder("");                          // tab-delimited output stats
	random = new Random(seed + (int)(10.0/(0.000001+cutoff))); // stream of pseudorandom numbers
	if ( 0 < cutoff ) cutoff += EPSILON;
	x025 = (int) Math.floor(CI_MIN * reps); while ( x025 < 0 ) ++x025;
	x975 = (int) Math.ceil(CI_MAX * reps);  while ( x975 >= reps ) --x975;
	

	//##########################################################################################################
	//### reading infile to build dm                                                                         ###
	//##########################################################################################################
	//### setting dfile and n (and lgt if ! dfile)
	in = new BufferedReader(new FileReader(infile));
	try { while ( (line=in.readLine().trim()).length() == 0 ) {} } catch ( NullPointerException e ) { System.err.println("empty input file (option -i)");  System.exit(1); }
	aS = splitFast(line, '\t'); //aS = line.split("\\s+");
	lgt = aS.length;
	dfile = (lgt == 1);
	n = 1;
	while ( true ) try { n = ( in.readLine().trim().length() == 0 ) ? n : ++n; } catch ( NullPointerException e ) { in.close(); break; }
	brows = readFields(rows, n); //## NOTE: reading rows
	//##########################################################################################################
	//### reading distance matrix                                                                            ###
	//##########################################################################################################
	if ( dfile ) {
	    System.err.print("reading file ...        ");
	    x = brows.cardinality();
	    lbl = new ArrayList<String>(x);
	    dm = new float[x][];
	    in = new BufferedReader(new FileReader(infile));
	    x = i = -1;
	    while ( ++x < n ) {     //## NOTE: n = total no. non empty lines
		try { line = in.readLine().trim(); } catch ( NullPointerException e ) { System.err.println("incorrect distance matrix file  (option -i)"); System.exit(1); }
		if ( line.length() == 0 ) { --x; continue; }
		if ( ! brows.get(x) ) continue;
		++i;                //## NOTE: i (= lbl id)  <=  x (= current id)
		o = (line + " ").indexOf(" ");
		lbl.add(line.substring(0, o));
		maxlbl = Math.max(maxlbl, lbl.get(i).length());
		dm[i] = splitRaw(line.substring(o).trim(), i, brows);
		/*dm[i] = new float[i];aS = line.substring(o).trim().split(" ");y = j = -1; while ( (y=brows.nextSetBit(++y)) != -1 && ++j < i ) 
		  try { dm[i][j] = Float.parseFloat(aS[y]); } catch ( NumberFormatException e ) { System.err.println("incorrect distance matrix file (option -i)"); System.exit(1); }*/
	    }
	    in.close();
	    System.err.println("[ok]");
	    n = dm.length;
	    outcaption = outcaption.append("n");              //## NOTE: updating outtab
	    outstats = outstats.append(n);                    //## NOTE: updating outtab
	    System.err.println("no. elements            " + n);
	}
	//##########################################################################################################
	//### reading profiles                                                                                   ###
	//##########################################################################################################
	else {
	    blfld = readFields(lfld, lgt); //## NOTE: reading lfld
	    bpfld = readFields(pfld, lgt); //## NOTE: reading pfld
	    n = brows.cardinality();
	    lbl = new ArrayList<String>(n);
	    lgt = bpfld.cardinality();
	    System.err.println("no. fields              " + lgt);
	    if ( nloc > 0 ) nloc = lgt;  //## NOTE: setting the true no. loci
	    pr = new short[n][lgt];
	    missing = new int[n];
	    System.err.print("reading file ...        ");
	    in = new BufferedReader(new FileReader(infile));
	    i = n = -1;
	    while ( (line=in.readLine()) != null ) {
		if ( line.trim().length() == 0 ) continue;
		++n;              //## NOTE: n = current profile id
		if ( ! brows.get(n) ) continue;
		++i;              //## NOTE: i = profile id <= n
		aS = splitFast(line, '\t');
		//## getting lbl  ##################
		sb = new StringBuilder("");
		x = -1; while ( (x=blfld.nextSetBit(++x)) != -1 ) sb = sb.append("_").append(aS[x]);
		lbl.add(sb.substring(1).replace(' ', '_'));
		maxlbl = ( maxlbl < (x=lbl.get(i).length()) ) ? x : maxlbl;
		//## getting profile  ##############
		x = j = -1;
		while ( (x=bpfld.nextSetBit(++x)) != -1 ) {
		    try { a = (short) Integer.parseInt(aS[x]); }      //## NOTE: values should be drawn from [1, 65535] !!!!!
		    catch ( NumberFormatException e ) {
			a = EMPTY;
			missing[i]++;
		    }
		    pr[i][++j] = a;
		}
	    }
	    in.close();
	    System.err.println("[ok]");
	    n = pr.length;
	    System.err.println("no. elements            " + n);
	    //### filtering profiles  #################################################################################
	    i = j = -1;
	    while ( ++i < n ) {
		rate = missing[i] / (double) lgt;
		if ( rate > missp ) {
		    System.err.println(String.format(Locale.US, "%.2f", 100*rate) + "% missing entries   discarding profile " + (++i) + " (" + lbl.get(--i) + ")");
		    continue;
		}
		if ( ++j < i ) {
		    pr[j] = Arrays.copyOf(pr[i], lgt);
		    missing[j] = missing[i];
		    lbl.set(j, lbl.get(i));
		}
	    }
	    if ( ++j < n ) {
		n = j;
		System.arraycopy(pr, 0, pr, 0, n);
		System.arraycopy(missing, 0, missing, 0, n);
		System.err.println("remaining elements      " + n);
	    }
	    outcaption = outcaption.append("n\tl");                    //## NOTE: updating outtab
	    outstats = outstats.append(n).append("\t").append(lgt);    //## NOTE: updating outtab
	    //### computing and writing distance matrix  ##############################################################
	    System.err.print("computing distances ... ");
	    dm = new float[n][];
	    si = new short[lgt];  //## NOTE: creating array for next System.copyOf
	    out = new BufferedWriter(new FileWriter(new File(basename + ".d")));
	    i = -1;
	    while ( ++i < n ) {
		out.write((lbl.get(i) + BLANK).substring(0, maxlbl));
		dm[i] = new float[i];
		System.arraycopy(pr[i], 0, si, 0, lgt);  //si = Arrays.copyOf(pr[i], lgt);  si = pr[i].clone(); // => slower
		j = -1;
		while ( ++j < i ) {
		    dij = ( (a_i=missing[i]) == 0 ) ?
			( missing[j] == 0 )  ? dpf(si, pr[j]) : dp(pr[j], si) :    //## NOTE: dpf(p1, p2) is faster than dp(p1, p2)
			( a_i < missing[j] ) ? dp(pr[j], si)  : dp(si, pr[j]);     //## NOTE: dp(p1, p2) is faster when missing[p1] > missing[p2]
		    if ( Float.isNaN(dij) ) {
			System.err.println("no comparable element between profiles " + (++j) + " (" + lbl.get(--j) + ") and " + (++i) + " (" + lbl.get(--i) + ")");
			System.err.println("try to select rows using option -r");
			out.close();
			System.exit(1);
		    }		    
		    dm[i][j] = dij;
		    out.write(String.format(Locale.US, " %.9f", dij));
		}
		out.newLine();
	    }
	    out.close();
	    System.err.println("[ok]");
	    System.err.println("distance matrix         " + basename + ".d");
	}	    

	
	//##########################################################################################################
	//### hierarchical and/or minimum spanning tree(s)                                                       ###
	//##########################################################################################################
	if ( cutoff < 0 ) { //## NOTE: no clustering
	    if ( hctree ) {
		System.err.print("hierarchical tree       ");
		out = new BufferedWriter(new FileWriter(new File(basename + ".nwk")));
		out.write(slinkHCT(dm, lbl));                                                          out.newLine();
		out.close();
		System.err.println(basename + ".nwk");
	    }
	    if ( mstree ) {
		System.err.print("minimum spanning tree   ");
		out = new BufferedWriter(new FileWriter(new File(basename + ".graphml")));
		out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");                               out.newLine();
		out.write("<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\">");                out.newLine();
		out.write(" <key id=\"d1\" for=\"edge\" attr.name=\"weight\" attr.type=\"double\"/>"); out.newLine();
		out.write(" <graph id=\"MST\" edgedefault=\"undirected\">");                           out.newLine();
		i = -1; while ( ++i < n ) { out.write("  <node id=\"" + lbl.get(i) + "\"/>");          out.newLine(); }
		a2i = mst(dm);
		b = n; --b;
		while ( --b >= 0 ) {
		    out.write("  <edge id=\"e" + (b+1)
			      + "\" source=\"" + lbl.get(i=a2i[b][0])
			      + "\" target=\"" + lbl.get(j=a2i[b][1]) + "\">");                        out.newLine();
		    out.write(String.format(Locale.US, "   <data key=\"d1\">%.9f</data>", dm[i][j]));  out.newLine();
		    out.write("  </edge>");                                                            out.newLine();
		}
		out.write(" </graph>");                                                                out.newLine();
		out.write("</graphml>");                                                               out.newLine();
		out.close();
		System.err.println(basename + ".graphml");
	    }
	    if ( outtab ) { 
		System.err.println(outcaption.toString());
		System.out.println(outstats.toString());
	    }
	    System.exit(0);
	}

	
	//##########################################################################################################
	//### clustering                                                                                         ###
	//##########################################################################################################
	System.err.print("clustering ...          ");
	clust = mstc(dm, cutoff);
	System.err.println("[ok]");
	k = psize(clust);
	outcaption = outcaption.append("\tc       \tk");                                                                   //## NOTE: updating outtab
	outstats = outstats.append(String.format(Locale.US, "\t%.9f\t", ((cutoff == 0) ? 0 : cutoff-EPSILON))).append(k);  //## NOTE: updating outtab
	System.err.println("no. classes             " + k);
	csize = csizes(clust);
	srt = new int[k][];           //## NOTE: srt[c][] = array of the elements in class c
	ai = new int[k];
	Arrays.fill(ai, -1);
	c = k;
	while ( --c >= 0 ) srt[c] = new int[csize[c]];
	i = n;
	while ( --i >= 0 ) {
	    c = clust[i];
	    ++ai[c];
	    srt[c][ai[c]] = i;
	}
	ai = null;
	//System.out.println(""); c = -1; while ( ++c < k ) System.out.println(Arrays.toString(srt[c]));
	

	//##########################################################################################################
	//### estimating silhouette indexes and sorting clusters                                                 ###
	//##########################################################################################################
	silh = silhouette(clust, dm, exponent);
	silhouette = avg(silh);
	outcaption = outcaption.append("\tsilhouette");                                      //## NOTE: updating outtab
	outstats = outstats.append(String.format(Locale.US, "\t%.6f", silhouette));          //## NOTE: updating outtab
	System.err.println("silhouette              " + String.format(Locale.US, "%.6f", silhouette));
	//## sorting each srt[c] according to the element silhouette indexes
	c = k;
	while ( --c >= 0 ) {
	    a2d = new double[x=csize[c]][2];
	    while ( --x >= 0 ) {
		i = srt[c][x];
		a2d[x][0] = silh[i];
		a2d[x][1] = i;
	    }
	    Arrays.sort(a2d, Comparator.comparingDouble(a -> -a[0]));
	    x = csize[c]; while ( --x >= 0 ) srt[c][x] = (int) a2d[x][1];
	}
	//## sorting srt according to the cluster silhouette indexes
	aS = new String[k];
	c = k;
	while ( --c >= 0 ) {
	    sb = new StringBuilder("");
	    up = 0; dn = x = csize[c];
	    while ( --x >= 0 ) {
		i = srt[c][x];
		up += silh[i];
		sb = sb.append(' ').append(String.valueOf(i));
	    }
	    line = ( up < 0 ) ? String.format(Locale.US, "%.17f%010d", up/dn, csize[c]) : String.format(Locale.US, "0%.17f%010d", up/dn, csize[c]); //## NOTE: silhouette (20 char) + size (10 char)
	    sb = sb.insert(0, line);
	    aS[c] = sb.toString();
	}
	Arrays.sort(aS);
	csilh = new double[k];
	c = k;
	for (String s: aS) {
	    aS2 = s.split(" ");
	    csilh[--c] = Double.parseDouble(aS2[0].substring(0,20));
	    x = aS2.length; srt[c] = new int[--x]; ++x;
	    y = -1; while ( --x > 0 ) srt[c][++y] = Integer.parseInt(aS2[x]); 
	}
	
	    
	//##########################################################################################################
	//### writing clustering                                                                                 ###
	//##########################################################################################################
	out = new BufferedWriter(new FileWriter(new File(basename + ".txt")));
	out.write("# n=" + n);                                                                        out.newLine();
	out.write("# c=" + String.format(Locale.US, "%.8f", cutoff));                                 out.newLine();
	out.write("# k=" + k);                                                                        out.newLine();
	out.write("# s=" + String.format(Locale.US, "%.8f", silhouette));                             out.newLine();
	c = -1;
	while ( ++c < k ) {                                                                           out.newLine();
	    out.write(("## cluster_" + (c+1) + BLANK).substring(0,15) + "  n=" + srt[c].length
		      + String.format(Locale.US, "   s=%." + ((csilh[c]<0)?7:8) + "f", csilh[c]));    out.newLine();
	    for (int i: srt[c]) {
		out.write((lbl.get(i)+BLANK).substring(0, maxlbl)
			  + String.format(Locale.US, "  s=%." + ((silh[i]<0)?7:8) + "f", silh[i]));   out.newLine();
	    }
	}
	out.close();
	System.err.println("clustering info         " + basename + ".txt");
		

	//##########################################################################################################
	//### hierarchical and/or minimum tree(s) after clustering                                               ###
	//##########################################################################################################
	if ( hctree || mstree ) {
	    aS = new String[n];
	    c = k; while ( --c >= 0 ) for (int i: srt[c]) aS[i] = "cluster_" + (c+1) + "___" + lbl.get(i);
	    if ( hctree ) {
		System.err.print("hierarchical tree       ");
		out = new BufferedWriter(new FileWriter(new File(basename + ".nwk")));
		out.write(slinkHCT(dm, new ArrayList<String>(Arrays.asList(aS))));                     out.newLine();
		out.close();
		System.err.println(basename + ".nwk");
	    }
	    if ( mstree ) {
		System.err.print("minimum spanning tree   ");
		out = new BufferedWriter(new FileWriter(new File(basename + ".graphml")));
		out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");                               out.newLine();
		out.write("<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\">");                out.newLine();
		out.write(" <key id=\"d1\" for=\"edge\" attr.name=\"weight\" attr.type=\"double\"/>"); out.newLine();
		out.write(" <graph id=\"MST\" edgedefault=\"undirected\">");                           out.newLine();
		i = -1; while ( ++i < n ) { out.write("  <node id=\"" + aS[i] + "\"/>");               out.newLine(); }
		a2i = mst(dm);
		b = n; --b;
		while ( --b >= 0 ) {
		    out.write("  <edge id=\"e" + (b+1)
			      + "\" source=\"" + aS[i=a2i[b][0]]
			      + "\" target=\"" + aS[j=a2i[b][1]] + "\">");                             out.newLine();
		    out.write(String.format(Locale.US, "   <data key=\"d1\">%.9f</data>", dm[i][j]));  out.newLine();
		    out.write("  </edge>");                                                            out.newLine();
		}
		out.write(" </graph>");                                                                out.newLine();
		out.write("</graphml>");                                                               out.newLine();
		out.close();
		System.err.println(basename + ".graphml");
	    }
	}


	//##########################################################################################################
	//### estimating noising statistics                                                                      ###
	//##########################################################################################################
	if ( nloc > 0 ) {
	    System.err.print("noising ...             ");
	    ++nloc;
	    aurand = new double[nloc];
	    aurand[--nloc] = 1;
	    rsi = new double[reps];        // silhouettes
	    rw1 = new double[reps];        // adjusted Wallace coef. 1
	    rw2 = new double[reps];        // adjusted Wallace coef. 2
	    r = -1;
	    while ( ++r < reps ) {
		x = nloc; while  ( --x >= 0 ) aurand[x] = random.nextDouble();
		Arrays.sort(aurand);                      // nloc sorted random values
		seed = random.nextLong();
		sdm = noisedm(dm, nloc, aurand);          // noised distance matrix
		noiclust = mstc(sdm, cutoff);             // clustering from the noised distances
		mm = abcd(contingency(noiclust, clust));  // mismatch matrix between the two partitions: noised vs. original
		rsi[r] = avg(silhouette(noiclust, sdm, exponent));
		aw = awal(mm);
		rw1[r] = aw[0];
		rw2[r] = aw[1];
	    }
	    System.err.println("[ok]");
	    Arrays.sort(rsi); lo = rsi[x025]; av = avg(rsi); up = rsi[x975];
	    outcaption = outcaption.append("\tnoise silhouette [low avg up] ");                     //## NOTE: updating outtab
	    outstats = outstats.append(String.format(Locale.US, "\t%.6f %.6f %.6f", lo, av, up));   //## NOTE: updating outtab
	    System.err.println("noise silhouette        " + String.format(Locale.US, "%.6f  [%.6f , %.6f]", av, lo, up));
	    Arrays.sort(rw1); lo = rw1[x025]; av = avg(rw1); up = rw1[x975];
	    outcaption = outcaption.append("\tnoise aWallace1 [low avg up] ");                      //## NOTE: updating outtab
	    outstats = outstats.append(String.format(Locale.US, "\t%.6f %.6f %.6f", lo, av, up));   //## NOTE: updating outtab
	    System.err.println("noise aWallace1         " + String.format(Locale.US, "%.6f  [%.6f , %.6f]", av, lo, up));
	    Arrays.sort(rw2); lo = rw2[x025]; av = avg(rw2); up = rw2[x975];
	    outcaption = outcaption.append("\tnoise aWallace2 [low avg up] ");                      //## NOTE: updating outtab
	    outstats = outstats.append(String.format(Locale.US, "\t%.6f %.6f %.6f", lo, av, up));   //## NOTE: updating outtab
	    System.err.println("noise aWallace2         " + String.format(Locale.US, "%.6f  [%.6f , %.6f]", av, lo, up));
	}


	if ( bins == 0 ) { //## NOTE: no subsampling analyses
	    if ( outtab ) { 
		System.err.println(outcaption.toString());
		System.out.println(outstats.toString());
	    }
	    System.exit(0);
	}

	
	//##########################################################################################################
	//### estimating subsampling statistics                                                                  ###
	//##########################################################################################################
	xaxis = new double[bins];      // abscissa values for subsampling curves
	ssi = new double[3][bins];     // subsampling silhouette distributions [2.5%CI, avg, 97.5%CI]
	sw1 = new double[3][bins];     // subsampling adjusted Wallace coef. 1 distributions [2.5%CI, avg, 97.5%CI]
	sw2 = new double[3][bins];     // subsampling adjusted Wallace coef. 2 distributions  [2.5%CI, avg, 97.5%CI]
	rsi = new double[reps];        // silhouettes
	rw1 = new double[reps];        // adjusted Wallace coef. 1
	rw2 = new double[reps];        // adjusted Wallace coef. 2
	step = 1.0 / (bins+1);
	rate = 0;
	b = -1;
	while ( ++b < bins ) {
	    xaxis[b] = (rate += step);
	    card = (int) Math.rint(rate * n);
	    System.err.print((("subsampling " + (b+1) + "/" + bins + " ...         ").substring(0, 23) + String.format(Locale.US, " %.1f", 100*rate) + "% (" + card + ")                    ").substring(0, 35));
	    r = -1;
	    while ( ++r < reps ) {
		seed = random.nextLong();
		subsample = subsample(n, rate, seed);         // array containing n*rate random element ids
		sdm = subdm(dm, subsample);                   // sub distance matrix corresponding to the subsampled elements
		clustsub = mstc(sdm, cutoff);                 // clustering of the subsampled elements
		subclust = subpartition(clust, subsample);    // restriction of the original clustering to the subsample elements
		mm = abcd(contingency(clustsub, subclust));   // mismatch matrix between the two partitions, clustsub vs. original
		rsi[r] = (1 + avg(silhouette(clustsub, sdm, exponent))) / 2;  //## NOTE: AUC are computed on (silhouette+1)/2, therefore varying between 0 and 1
		aw = awal(mm);
		rw1[r] = aw[0];
		rw2[r] = aw[1];
	    }
	    Arrays.sort(rsi); ssi[0][b] = rsi[x025]; ssi[1][b] = avg(rsi); ssi[2][b] = rsi[x975];
	    System.err.print(String.format(Locale.US, "   S %.4f [%.4f , %.4f]", 2*ssi[1][b]-1, 2*ssi[0][b]-1, 2*ssi[2][b]-1));
	    Arrays.sort(rw1); sw1[0][b] = rw1[x025]; sw1[1][b] = avg(rw1); sw1[2][b] = rw1[x975];
	    System.err.print(String.format(Locale.US, "  aW1 %.4f [%.4f , %.4f]", sw1[1][b], sw1[0][b], sw1[2][b]));
	    Arrays.sort(rw2); sw2[0][b] = rw2[x025]; sw2[1][b] = avg(rw2); sw2[2][b] = rw2[x975];
	    System.err.println(String.format(Locale.US, "  aW2 %.4f [%.4f , %.4f]", sw2[1][b], sw2[0][b], sw2[2][b]));
	}
	lo = nauc(xaxis, ssi[0]); av = nauc(xaxis, ssi[1]); up = nauc(xaxis, ssi[2]);
	outcaption = outcaption.append("\tnAUC silhouette [low avg up] ");                      //## NOTE: updating outtab
	outstats = outstats.append(String.format(Locale.US, "\t%.6f %.6f %.6f", lo, av, up));   //## NOTE: updating outtab
	System.err.println("nAUC silhouette         " + String.format(Locale.US, "%.6f  [%.6f , %.6f]", av, lo, up));
	lo = nauc(xaxis, sw1[0]); av = nauc(xaxis, sw1[1]); up = nauc(xaxis, sw1[2]);
	outcaption = outcaption.append("\tnAUC aWallace1 [low avg up] ");                       //## NOTE: updating outtab
	outstats = outstats.append(String.format(Locale.US, "\t%.6f %.6f %.6f", lo, av, up));   //## NOTE: updating outtab
	System.err.println("nAUC aWallace1          " + String.format(Locale.US, "%.6f  [%.6f , %.6f]", av, lo, up));
	lo = nauc(xaxis, sw2[0]); av = nauc(xaxis, sw2[1]); up = nauc(xaxis, sw2[2]);
	outcaption = outcaption.append("\tnAUC aWallace2 [low avg up]");                        //## NOTE: updating outtab
	outstats = outstats.append(String.format(Locale.US, "\t%.6f %.6f %.6f", lo, av, up));   //## NOTE: updating outtab
	System.err.println("nAUC aWallace2          " + String.format(Locale.US, "%.6f  [%.6f , %.6f]", av, lo, up));

	if ( outtab ) { 
	    System.err.println(outcaption.toString());
	    System.out.println(outstats.toString());
	}
    }

    //##########################################################################################################
    //##########################################################################################################
    //### static methods                                                                                     ###
    //##########################################################################################################
    //##########################################################################################################
    
    // reads field(s) and returns field id(s) in a BitSet
    final static BitSet readFields(final String fields, final int size) {
	int x = 0, y, z;
    	BitSet bs = new BitSet(size);
	for (String s: fields.trim().split(",")) {
	    if ( (z=s.indexOf("-")) == -1 ) {
		try { x = Integer.parseInt(s); }              catch ( NumberFormatException e ) { System.err.println("incorrect field definition: " + s); System.exit(1); }
		if ( --x < 0 )                                                                  { System.err.println("incorrect field definition: " + s); System.exit(1); }
		bs.set(x);
		continue;
	    }
	    try { x = Integer.parseInt(s.substring(0,z)); }   catch ( NumberFormatException e ) { System.err.println("incorrect field definition: " + s); System.exit(1); }
	    if ( --x < 0 )                                                                      { System.err.println("incorrect field definition: " + s); System.exit(1); }
	    y = size;
	    if ( ++z < s.length() )
		try { y = Integer.parseInt(s.substring(z)); } catch ( NumberFormatException e ) { System.err.println("incorrect field definition: " + s); System.exit(1); }
	    if ( y <= x )                                                                       { System.err.println("incorrect field definition: " + s); System.exit(1); }
	    bs.set(x, y);
	}
	//System.out.println(fields); System.out.println(bs.toString());
	return bs;
    }

    // fast implementation of line.split(delimiter)
    final static String[] splitFast(final String line, final char delimiter) {
	String[] a = new String[line.length()];
	int x = 0, y = -1, i = -1;
	while ( (y=line.indexOf(delimiter, (x=++y))) >= 0 ) a[++i] = line.substring(x, y);
	a[++i] = line.substring(x);
	return Arrays.copyOf(a, ++i);
    }	

    // returns a float array of specified size by splitting the specified String using (unique) blank space field separator and getting only specified fields
    // faster than line.split(" ")
    final static float[] splitRaw(final String line, final int size, final BitSet fields) {
	float[] a = new float[size];
	int x = 0, y = -1, i = -1, j = -1;
	while ( ++j < size  &&  (y=line.indexOf(' ', (x=++y))) >= 0 ) 
	    a[j] = ( fields.get(++i) ) ? (float) Double.parseDouble(line.substring(x, y)) : a[j--];
	    //if ( fields.get(++i) ) a[j] = (float) Double.parseDouble(line.substring(x, y)) else --j;
	if ( j < size  &&  fields.get(++i) ) a[j] = (float) Double.parseDouble(line.substring(x));
	return a;
    }

     // returns the pairwise distance between specified profiles without empty entry
    final static float dpf(final short[] p1, final short[] p2) {
	int up = 0, m = Math.min(p1.length, p2.length); final double dn = m;
	while ( --m >= 0 && ( p1[m] == p2[m] || ++up > 0 ) ) {}
	//while ( --m >= 0 ) up += ( p1[m] != p2[m] ) ? 0b1 : 0;
	return (float) (up / dn);
    }    
    
    // returns the pairwise distance between specified profiles
    final static float dp(final short[] p1, final short[] p2) {
	int a1, a2, m = ((a1=p1.length) < (a2=p2.length)) ? a1 : a2, dn = m, up = 0;
	while ( --m >= 0 ) up += ( (a1=p1[m]) == EMPTY || (a2=p2[m]) == EMPTY ) ? (--dn) * 0 : ( a1 != a2 ) ? 0b1 : 0;              //## fast
	return (float) (up / (double) dn);
    }    
	
    // returns the average of the specified array values
    final static double avg(final double[] ar) {
	double up = 0, dn = 0;
	for (double x: ar) if ( ! Double.isNaN(x) && ! Double.isInfinite(x) ) { up += x; dn++; }
	return up / dn ;
    }

    // computes a single-linkage hierarchical tree
    final static String slinkHCT(final float[][] dm, final ArrayList<String> label) {
 	int n = dm.length;                           // size
	float[][] d = Arrays.copyOf(dm, n);          // dissimilarity copy
	String[] c = label.toArray(new String[n]);   // clades
	double[] height = new double[n];             // clade heights 
	int[] csize = new int[n];                    // clade sizes
	Arrays.fill(csize, 1);
	double min, minmin = 0; int i, j, x, y, dn = n;
	while ( --dn > 0 ) {
	    x = y = -1; min = Double.POSITIVE_INFINITY; i = n;
	    while ( --i >= 0 && min > minmin ) if ( csize[j=i] > 0 ) while ( --j >= 0 ) min = ( csize[j] > 0 && dm[i][j] < min ) ? dm[x=i][y=j] : min;  //min /= 2;    // searching for x and y to agglomerate
	    minmin = ( min > minmin ) ? min : minmin; i = n;
	    while ( --i >= 0 ) if ( i != x && i != y && csize[i] > 0 ) d[(i<x)?x:i][(i<x)?i:x] = Math.min(d[(i<x)?x:i][(i<x)?i:x], d[(i<y)?y:i][(i<y)?i:y]);           // updating dm between parent(x,y) and every i != x,y
	    c[x] = (csize[x] > csize[y])                                                                                                                               // replacing x by the subtree (x,y)
		? String.format(Locale.US, "("+c[x]+":%.8f,"+c[y]+":%.8f)", min-height[x], min-height[y])
		: String.format(Locale.US, "("+c[y]+":%.8f,"+c[x]+":%.8f)", min-height[y], min-height[x]);
	    csize[x] += csize[y];
	    height[x] = min;
	    c[y] = null;
	    csize[y] = 0;
	}
	d = null;
	height = null;
	csize = null;
	return c[--n];
    }
    
    // computes the MST-based clustering with specified (inclusive) cutoff using a Kruskall-like algorithm
    // returns the partitions within an array
    final static int[] mstc(final float[][] dm, final double cutoff) {
	int n = dm.length;               // no. elements
	BitSet[] c = new BitSet[n];      // clusters
	int[] p = new int[n];            // partition id for each element
	int ne = (--n) * (++n) / 2;      // no. edges
	//## parsing the edge of the dm clique
	float[][] e = new float[ne][];   // edge list
	float dij; int j, y, x = -1, i = n;
	while ( (j=--i) >= 0 ) {
	    c[i] = new BitSet(n);
	    c[i].set(i);
	    p[i] = i;
	    while ( --j >= 0 )
		if ( (dij=dm[i][j]) <= cutoff ) { //## NOTE: cutoff is inclusive
		    e[++x] = new float[3];
		    e[x][0] = i;
		    e[x][1] = j;
		    e[x][2] = dij;
		}
	}
	ne = ++x;
	e = Arrays.copyOf(e, ne);
	Arrays.sort(e, Comparator.comparingDouble(a -> a[2]));
	//## Kruskall
	BitSet b = new BitSet(n); b.set(0, n);    // active classes
	x = -1;
	while ( ++x < ne ) 
	    if ( (i=p[(int)e[x][0]]) != (j=p[(int)e[x][1]]) ) {  //## NOTE: j = j U i, and next discarding i
		y = -1; while ( (y=c[i].nextSetBit(++y)) >= 0 ) p[y] = j;
		c[j].or(c[i]);
		b.clear(i);
	    }
	c = null;
	e = null;
 	//System.out.println(Arrays.toString(p));
	//## rewriting partition ids	
	int[] rwr = new int[b.cardinality()];
	i = j = -1; while ( (i=b.nextSetBit(++i)) >= 0 ) rwr[++j] = i;
	while ( --n >= 0 ) p[n] = Arrays.binarySearch(rwr, p[n]);
	b = null;
	rwr = null;
 	//System.out.println(Arrays.toString(p));
	return p;
    }

    // computes a MST using Prim algorithm
    // return an array of n-1 tuples (x, y) with x > y
    final static int[][] mst(final float[][] dm) {
	int n = dm.length;           // no. elements
	BitSet v = new BitSet(n);    // processed vertices
	int ne = n; --ne;            // no. edges
	int[][] t = new int[ne][2];  // mstree
	v.set(0);
	float min; int x, y, j, i, e = -1;
	while ( ++e < ne ) {
	    min = Float.MAX_VALUE; i = x = y = -1;
	    while ( (i=v.nextSetBit(++i)) >= 0 ) {
		j = n; while ( (j=v.previousClearBit(--j)) >= 0 ) min = ( j < i ) ? ( dm[i][j] < min ) ? dm[x=i][y=j] : min : ( dm[j][i] < min ) ? dm[x=j][y=i] : min;
	    }
	    t[e][0] = x; v.set(x);
	    t[e][1] = y; v.set(y);
	}
	return t;
    }   

    // returns a matrix of noised distances from the specified distance matrix dm
    // noised distances lgt * dn ~ B(dm, lgt)
    // see e.g. doi:10.1080/01621459.1972.10481259
    final static float[][] noisedm(final float[][] dm, final int lgt, final double[] sortrand) {
	Random rand = new Random(seed);
	int n = dm.length;
	float[][] dn = new float[n][];
	double dij, l = lgt; int x, j, i = n;
	while ( (j=--i) >= 0 ) {
	    dn[i] = new float[i];
	    while ( --j >= 0 ) {
		dn[i][j] = ( (dij=dm[i][j]) == 1.0f ) ? 1.0f : ( dij == 0 ) ? 0 : ( (x=Arrays.binarySearch(sortrand, dij)) < 0 ) ? (float) (-(++x)/l) : (float) (x/l);
		//System.out.println(dij + " " + dn[i][j]);
	    }
	}
	return dn;
    }
    
    // returns an array containing rate*size sorted integers randomly drawn in [0,size[
    final static int[] subsample(final int size, final double rate, final long seed) {
	Random rand = new Random(seed);
	BitSet b = new BitSet(size);
	int card = (int) Math.rint(rate * size);
	int i = card; while ( --i >= 0 ) b.set(rand.nextInt(size));
	while ( b.cardinality() < card ) b.set(rand.nextInt(size));
	int[] s = new int[card];
	i = card = -1; while ( (i=b.nextSetBit(++i)) != -1 ) s[++card] = i;
	b = null;
	//System.out.println(Arrays.toString(s));
	return s;
    }
    
    // returns a submatrix of distances from the specified row ids
    final static float[][] subdm(final float[][] dm, final int[] row) {
	int n = row.length;
	float[][] sm = new float[n][];
	int j, i = n; while ( --i >= 0 ) sm[i] = new float[i];
	i = n; while ( (j=--i) >= 0 ) while ( --j >= 0 ) sm[i][j] = dm[row[i]][row[j]];
	return sm;
    }
	    
    // returns a subpartition from the specified row ids
    final static int[] subpartition(final int[] partition, final int[] row) {
	int n = row.length;
	int[] sp = new int[n];
	TreeSet<Integer> ts = new TreeSet<Integer>();
	while ( --n >= 0 ) ts.add((sp[n]=partition[row[n]]));
	int[] rwr = new int[ts.size()];
	n = -1; for (Integer i: ts) rwr[++n] = i;
	ts = null;
	n = sp.length; while ( --n >= 0 ) sp[n] = Arrays.binarySearch(rwr, sp[n]);
	rwr = null;
	return sp;
    }

    // returns the no. partitions in a specified partition array
    final static int psize(final int[] partition) {
	TreeSet<Integer> ts = new TreeSet<Integer>();
	for (int x: partition) ts.add(x);
	return ts.size();
    }    

    // returns the size of each class from a specified partition array
    final static int[] csizes(final int[] partition) {
	int k = psize(partition);
	int[] cs = new int[k];
	for (int c: partition) cs[c]++;
	return cs;
    }    

    // returns the silhouette index for every element using the generalized mean for the specified partition array from the specified distance matrix
    final static double[] silhouette(final int[] partition, final float[][] dm, final int exponent) {
	int n = partition.length;        // no. elements
	double[] s = new double[n];
	int k = psize(partition);        // no. classes
	if ( k == 1 ) return s;
	int[] csize = csizes(partition); // size of each class
	double[][] d = new double[n][k]; // d[i][c] = mean distance between element i and class c
	double ai, bi, dic; int c, ci, cj, j, i = n;
	while ( (j=--i) >= 0 ) {
	    switch ( exponent ) {
	    case NINF: Arrays.fill(d[i], Double.MAX_VALUE); break;
	    case PINF: Arrays.fill(d[i], Double.MIN_VALUE); break;
	    }
	    ci = partition[i];
	    while ( --j >= 0 ) {
		cj = partition[j]; dic = dm[i][j];
		switch ( exponent ) {
		case -1:   d[i][cj] += (ai=(1.0/dic));                    d[j][ci] += ai;                                break; //## NOTE: harmonic mean
	      //case 0:    d[i][cj] *= dic;                               d[j][ci] *= dic;                               break; //## NOTE: geometric mean
		case 0:    d[i][cj] += (ai=Math.log(dic+DELTA));          d[j][ci] += ai;                                break; //## NOTE: geometric mean
		case 1:    d[i][cj] += dic;                               d[j][ci] += dic;                               break; //## NOTE: arithmetic mean
		case 2:    d[i][cj] += (ai=(dic*dic));                    d[j][ci] += ai;                                break; //## NOTE: quadratic mean
		case NINF: d[i][cj] = ( (ai=d[i][cj]) < dic ) ? ai : dic; d[j][ci] = ( (ai=d[j][ci]) < dic ) ? ai : dic; break; //## NOTE: minimum
		case PINF: d[i][cj] = ( (ai=d[i][cj]) > dic ) ? ai : dic; d[j][ci] = ( (ai=d[j][ci]) > dic ) ? ai : dic; break; //## NOTE: maximum
		default:   d[i][cj] += (ai=Math.pow(dic, exponent));      d[j][ci] += ai;                                break; //## NOTE: power mean
		}
	    }
	}
	i = n;
	while ( --i >= 0 ) {
	    ci = partition[i];
	    if ( csize[ci] == 1 ) continue;
	    ai = bi = Double.POSITIVE_INFINITY; c = k;
	    switch ( exponent ) {
	    case -1:   ai = (csize[ci]-1) / d[i][ci];                       while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=(csize[c]/d[i][c])) )                       ? dic : bi; break; //## NOTE: harmonic mean
	  //case 0:    ai = Math.pow(d[i][ci], 1.0/(csize[ci]-1));          while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=Math.pow(d[i][c], 1.0/csize[c])) )          ? dic : bi; break; //## NOTE: geometric mean
	    case 0:    ai = Math.exp(d[i][ci]/(csize[ci]-1)) - DELTA;       while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=(Math.exp(d[i][c]/csize[c])-DELTA)) )       ? dic : bi; break; //## NOTE: geometric mean
	    case 1:    ai = d[i][ci] / (csize[ci]-1);                       while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=(d[i][c]/csize[c])) )                       ? dic : bi; break; //## NOTE: arithmetic mean
	    case 2:    ai = Math.sqrt(d[i][ci] / (csize[ci]-1));            while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=Math.sqrt(d[i][c]/csize[c])) )              ? dic : bi; break; //## NOTE: quadratic mean
	    case NINF:
	    case PINF: ai = d[i][ci];                                       while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=d[i][c]) )                                  ? dic : bi; break; //## NOTE: maximum/minimum
	    default:   ai = Math.pow(d[i][ci]/(csize[ci]-1), 1.0/exponent); while ( --c >= 0 ) if ( c != ci ) bi = ( bi > (dic=Math.pow(d[i][c]/csize[c], 1.0/exponent)) ) ? dic : bi; break; //## NOTE: power mean
	    }
	    s[i] = (ai == bi) ? 0 : (ai < bi) ? 1 - ai/bi : bi/ai - 1;
	}
	csize = null;
	d = null;
	return s;
    }
    
    // returns a contingency table from the two specified partitions on the same set (Table 1 in Hubert and Arabie 1985 doi:10.1007/bf01908075)
    // see http://www.comparingpartitions.info/index.php?link=Tut14
    final static int[][] contingency(final int[] p1, final int[] p2) {
	int[][] c = new int[psize(p1)][psize(p2)];
	int i = p1.length;
	while ( --i >= 0 ) c[p1[i]][p2[i]]++;
	return c;
    }

    // returns an array corresponding to the 2x2 mismatch matrix estimated from the specified contingency matrix (Table 2 in Hubert and Arabie 1985 doi:10.1007/bf01908075)
    final static int[] abcd(final int[][] ctg) {
	int nr = ctg.length;     // no. rows
	int nc = ctg[0].length;  // no. col.
	int[] sr = new int[nr];  // row sum
	int[] sc = new int[nc];  // col sum
	int s0 = 0, s1 = 0, s2 = 0, s3 = 0, x, j, i = nr;
	while ( --i >= 0 && (j=nc) > 0 ) 
	    while ( --j >= 0 ) {
		sr[i] += (x=ctg[i][j]);
		sc[j] += x;
		s0 += x;
		s1 += x * x;
	    }
	for (int s: sr) s2 += s * s;
	for (int s: sc) s3 += s * s;
	int[] abcd = new int[4];
	abcd[0] = Math.abs(s1 - s0) / 2;
	abcd[1] = Math.abs(s2 - s1) / 2;
	abcd[2] = Math.abs(s3 - s1) / 2;
	abcd[3] = Math.abs(s0*s0 + s1 - s2 - s3) / 2;
	return abcd;
    }		
    
    // Rand index (Rand 1971, doi:10.2307/2284239)
    final static double rand(final int[][] ctg) { return rand(abcd(ctg)); }
    final static double rand(final int[] mm) {
	return (mm[0] + mm[3]) / (double) (mm[0] + mm[1] + mm[2] + mm[3]);
    }

    // adjusted Rand index (Hubert and Arabie 1985 doi:10.1007/bf01908075)
    final static double arand(final int[][] ctg) { return arand(abcd(ctg)); }
    final static double arand(final int[] mm) {
	double a = mm[0], b = mm[1], c = mm[2], d = mm[3], n = a + b + c + d;
	return ( n == a || n == d ) ? 1 : 2 * (a * d - b * c) / ((a + b) * (b + d) + (a + c) * (c + d));
    }

    // Wallace coefficients (Wallace 1983 doi:10.2307/2288118)
    final static double[] wal(final int[][] ctg) { return wal(abcd(ctg)); }
    final static double[] wal(final int[] mm) {
	double a = mm[0], b = mm[1], c = mm[2];
	double[] w = new double[2];
	w[0] = ( a == 0 ) ? 0 : ( b == 0 ) ? 1 : a / (a + b);  // Wallace(row -> col)
	w[1] = ( a == 0 ) ? 0 : ( c == 0 ) ? 1 : a / (a + c);  // Wallace(col -> row)
	return w;
    }
	
    // adjusted Wallace coefficients (Severiano et al. 2011 doi:10.1128/JCM.00624-11)
    final static double[] awal(final int[][] ctg) { return awal(abcd(ctg)); }
    final static double[] awal(final int[] mm) {
	double a = mm[0], b = mm[1], c = mm[2], d = mm[3], n = a + b + c + d;
	double[] aw = new double[2];
	aw[0] = ( a == 0 ) ? 0 : ( a == n || b == 0 ) ? 1 : (a * d - b * c) / ((a + b) * (b + d));
	aw[1] = ( a == 0 ) ? 0 : ( a == n || c == 0 ) ? 1 : (a * d - b * c) / ((a + c) * (c + d));
	if ( Double.isInfinite(aw[0]*aw[1]) || Double.isNaN(aw[0]*aw[1]) || aw[0] < 0 || aw[1] < 0 ) System.out.println("\n" + a + " " + b + " " + c + " " + d + "   " + aw[0] + " " + aw[1]);
	return aw;
    }
   
    // returns the area under the specified curve with min >= 0
    final static double auc(final double[] x, final double[] y) {
	int b = 0, size = Math.min(x.length, y.length);
	double auc = 0, x1 = x[b], y1 = y[b], x2, y2;
	while ( ++b < size ) {
	    x2 = x[b]; y2 = y[b];
	    auc += (x2 - x1) * (y1 + y2);
	    x1 = x2; y1 = y2;
	}
	return auc / 2;
    }
	
    // returns the normalized area under the specified curve with min=0 and max=1
    final static double nauc(final double[] x, final double[] y) {
	return auc(x, y) / ( x[x.length-1] - x[0] );
    }
	
}
	
